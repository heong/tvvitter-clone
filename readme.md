Project MVC architecture overview :

 * urls(response function):
		'/','/admin','/login','/logout','/signup','/submit','/tweets',
		'/users','/users/(\w{0,30}','/follow'

 * forms(data exchange formats):
		'UserCreateForm','AuthenticateForm','Tweet'

 * models(database tables):
		'User','Tweet','UserProfile'


See the demo at [http://test-bm21n2sg37indbe70pod.c9users.io](http://test-bm21n2sg37indbe70pod.c9users.io/)
