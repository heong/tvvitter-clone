from django.contrib import admin

# Register your models here.

from .models import Tweet, UserProfile, User

admin.site.register(Tweet)
admin.site.register(UserProfile)
