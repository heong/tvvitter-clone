"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
import twitterApp.views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', twitterApp.views.index), # root
    url(r'^login$', twitterApp.views.login_view), # login
    url(r'^logout$', twitterApp.views.logout_view), # logout
    url(r'^signup$', twitterApp.views.signup), # signup
    url(r'^tweets$', twitterApp.views.public), # public tweets
    url(r'^submit$', twitterApp.views.submit), # submit new tweet
    url(r'^users/$', twitterApp.views.users),
    url(r'^users/(?P<username>\w{0,30})/$', twitterApp.views.users),
    url(r'^follow$', twitterApp.views.follow),
]
